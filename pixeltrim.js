(function ($) {

  Drupal.behaviors.pixeltrimAwesomeo = {
    attach: function(context, settings) {
      var selectors = settings.pixeltrim;
      jQuery.each(selectors, function(key, selector){
        $(selector.selector).each(function(){
          var selector_width = $(this).width(); // .node h2 a

					// added from home saturday night, to search down to last child in selector
					//and get the html content from that instead of just the selector. not tested yet...
//					var sel = $(this);
//					while(sel.children().length > 0) {
//					  sel = $(sel.children().get(0));
//					}
//          var selector_text = sel.html();
					// **********************************************************************************

         var selector_text = $(this).html();

          var trimmed = selector_text;
          var tmp = selector_text;
          if (selector_width > selector.width) {
            trimmed += "...";
            while ($(this).html(trimmed).width() > selector.width) {
              tmp = tmp.substring(0, tmp.length-1);
              trimmed = tmp + "...";
            }
            $(this).html(trimmed);
          }
        });
      });
    }
  };
})(jQuery);
